const R = require('ramda');
const responses = require('./responses');

const POSITIVE = /(sure|yes|yup|sounds? good)/i;

const notEmpty = R.complement(R.isEmpty);
const notNil = R.complement(R.isNil);

/*
 * ----------------------------------------------------------------------------
 * Matchers
 * Very naive set of predicates that test against a given context and input.
 * Signature: (context: Object, input: String) -> Boolean
 */

const defaultCase = () => true;

/* Should we offer initiating the booking process? */
const wantsToBook = (context, input) =>
  /(rent|book|get|make).*(suite|room|reservation)/i.test(input);

/* Should we answer questions about expenses and offer booking? */
const asksAboutExpenses = (context, input) =>
  /(how|what).*(much|price|pay|expensive|expenses)/i.test(input);

/* Should we answer questions about view and offer booking? */
const asksAboutView = (context, input) =>
  /(can|would).*(see|view|observe).*(brandenburger tor|leipziger platz|fernsehturm|unter den linden)/i.test(input);

/* Should we describe the journey */
const asksAboutJourney = (context, input) =>
  /(get (to)?|public).*(hotel|there|adlon|transportation)/i.test(input);

/* Should we describe our smoking policy? */
const asksAboutSmoking = (context, input) =>
  /(smoke(ing)?)/i.test(input);

/* Should we describe our pet policy? */
const asksAboutPets = (context, input) =>
  /(bring|have).*(pet|dog|cat|animal|fish|bird)/i.test(input);

/* Should we describe our wifi policy? */
const asksAboutWifi = (context, input) =>
  /(internet|wi[ -]?fi)/i.test(input);

/* Should we prompt for check-in? */
const shouldPromptForCheckin = (context, input) =>
  POSITIVE.test(input) && context.offeredBooking;

/* Should we cancel the order? */
const shouldCancel = (context, input) =>
  /cancel/.test(input) && context.isBooking;

/* Should we prompt for check-out? */
const shouldPromptForCheckOut = (context, input) =>
  notEmpty(input) && context.isBooking;

/* Should we prompt for room number? */
const shouldPromptForRooms = (context, input) =>
  notEmpty(input) && context.isBooking && notNil(context.checkIn);

/* Should we prompt for adults? */
const shouldPromptForAdults = (context, input) =>
  notEmpty(input)
    && (context.isBooking)
    && notNil(context.checkIn)
    && notNil(context.checkOut);

/* Should we prompt for children? */
const shouldPromptForChildren = (context, input) =>
  notEmpty(input)
    && context.isBooking
    && notNil(context.checkIn)
    && notNil(context.checkOut)
    && notNil(context.numberOfRooms);

/* Should we prompt for type of room? */
const shouldPromptForTypeOfRoom = (context, input) =>
  notEmpty(input)
    && context.isBooking
    && notNil(context.checkIn)
    && notNil(context.checkOut)
    && notNil(context.numberOfRooms)
    && notNil(context.adults);

/* Should we ask for confirmation of the order? */
const shouldPromptConfirmation = (context, input) =>
  notEmpty(input)
    && context.isBooking
    && notNil(context.checkIn)
    && notNil(context.checkOut)
    && notNil(context.numberOfRooms)
    && notNil(context.adults)
    && notNil(context.children);

/* Should we ask for confirmation of the order? */
const shouldConfirmOrder = (context, input) =>
  notEmpty(input)
    && context.isBooking
    && notNil(context.checkIn)
    && notNil(context.checkOut)
    && notNil(context.numberOfRooms)
    && notNil(context.adults)
    && notNil(context.children)
    && notNil(context.typeOfRoom);

/* Is Alice greeting us? */
const isGreeting = (context, input) =>
  /(hey|hi|hello|good day|hallo|how are you|hel l o)/i.test(input);

/*
 * ----------------------------------------------------------------------------
 * Producers
 * Take context and input and generate new context and response.
 * Signature: (context: Object, input: String) -> {context: Object, response: String}
 */

/* Offer to initiate the booking process. */
const offerBooking = R.always({
  context: {offeredBooking: true},
  response: responses.offerBooking,
});

/* Cancel the booking process. */
const cancel = R.always({
  context: {isBooking: false},
  response: responses.cancel,
});

/* Prompt Alice for her check-in */
const promptCheckIn = R.always({
  context: {offeredBooking: false, isBooking: true},
  response: responses.promptCheckIn,
});

/* Prompt Alice for her check-out, update check-in */
const promptCheckOut = (context, input) => ({
  context: {isBooking: true, checkIn: input},
  response: responses.promptCheckOut,
});

/* Prompt Alice for number of rooms, update check-out */
const promptRooms = (context, input) => ({
  context: Object.assign(context, {checkOut: input}),
  response: responses.promptRooms,
});

/* Prompt Alice for number of adults, update #rooms */
const promptAdults = (context, input) => ({
  context: Object.assign(context, {numberOfRooms: input}),
  response: responses.promptAdults,
});

/* Prompt Alice for number of children, update adults */
const promptChildren = (context, input) => ({
  context: Object.assign(context, {adults: input}),
  response: responses.promptChildren,
});

/* Prompt Alice for type of room, update children */
const promptTypeOfRoom = (context, input) => ({
  context: Object.assign(context, {children: input}),
  response: responses.promptTypeOfRoom,
});

/* Prompt Alice for confirmation, update type of room */
const promptConfirmation = (context, input) => ({
  context: Object.assign(context, {typeOfRoom: input}),
  response: responses.promptConfirmation,
});

/* Confirm order, reset context */
const confirmOrder = () => ({
  context: {isBooking: false},
  response: responses.confirmOrder,
});

/* Kindly greet Alice */
const greet = (context) => ({
  context,
  response: responses.greeting
});

/* Tell about journey to the hotel */
const journey = (context) => ({
  context,
  response: responses.journey
});

/* Tell about expenses  */
const expenses = () => ({
  context: {offeredBooking: true},
  response: responses.expenses
});

/* Tell about view */
const view = () => ({
  context: {offeredBooking: true},
  response: responses.view
});

/* Tell about our pet policy */
const petPolicy = context => ({context, response: responses.petPolicy});

/* Tell about our smoking policy */
const smokingPolicy = context => ({context, response: responses.smokingPolicy});

/* Tell about our wifi policy */
const wifi = context => ({context, response: responses.wifi});

/* Signal that we don't understand Alice' intent */
const dontUnderstand = R.always({context: {}, response: responses.dontUnderstand});

/**
 * Tests given context and user input against set of matchers,
 * mapping onto a set of producers which generate a new context
 * and response.
 */
const match = (context, input) => R.cond([
  [asksAboutWifi, wifi],
  [asksAboutSmoking, smokingPolicy],
  [asksAboutPets, petPolicy],
  [asksAboutView, view],
  [asksAboutExpenses, expenses],
  [asksAboutJourney, journey],
  [shouldCancel, cancel],
  [shouldConfirmOrder, confirmOrder],
  [shouldPromptConfirmation, promptConfirmation],
  [shouldPromptForTypeOfRoom, promptTypeOfRoom],
  [shouldPromptForChildren, promptChildren],
  [shouldPromptForAdults, promptAdults],
  [shouldPromptForRooms, promptRooms],
  [shouldPromptForCheckOut, promptCheckOut],
  [shouldPromptForCheckin, promptCheckIn],
  [wantsToBook, offerBooking],
  [isGreeting, greet],
  [defaultCase, dontUnderstand],
])(context, input);

module.exports = match;
