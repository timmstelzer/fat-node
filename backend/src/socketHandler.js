const match = require('./match');

/**
  * The SocketHandler class handles sockets
  */
class SocketHandler {
  /**
    * Creates a new SocketHandler
    */
  constructor() {
    this.context = {};
  }

  /**
    * Logs that a user connected
    */
  onConnect(/* istanbul ignore next */{c = console} = {}){
    c.log('a user connected');
  }

  /**
    * Logs that a user disconnected
    */
  onDisconnect(/* istanbul ignore next */{c = console} = {}){
    c.log('a user disconnected');
  }

  /**
    * Responds to a message
    */
  onMessage(/* istanbul ignore next */{msg, socket, c = console} = {}){
    c.log('message: ' + msg);

    const {response, context} = match(this.context, msg);

    this.context = context;

    socket.emit('chat message', response);
  }

}

exports.SocketHandler = SocketHandler;
