module.exports = {
  dontUnderstand: 'Sorry, I do not understand this',
  greeting: 'Hey Alice',
  smokingPolicy: `We offer a smokers lounge on the ground floor next to the reception, and
the Sra Bua Bar is open for smokers, but the rest of the hotel is no-smoking.`,
  petPolicy: `Dogs are allowed in the hotel and can be brought along. We will
  charge a 150 € cleaning fee in addition to the room rate, though dogs are not
  allowed in the restaurants.`,
  wifi: 'Wi-Fi is available throughout the hotel.',
  offerBooking: `Let's walk through booking and find a room that suites you
  best! You can abort the process at any time by saying the word 'cancel'.
  You will get a chance to review your selection at the end. Would that be alright with you?`,
  promptCheckIn: 'Great, when would you like to check-in?',
  promptCheckOut: 'And when would you like to check-out?',
  promptRooms: 'Awesome, and how many rooms would you like to have?',
  promptAdults: 'How many adults will stay in your room(s)?',
  promptChildren: 'And how many children are with you?',
  promptTypeOfRoom: `Great! Now on to the room itself. We offer these:
  (1) DELUXE ROOM
  (2) SUPERIOR DELUXE ROOM
  (3) EXECUTIVE ROOM
  (4) JUNION SUITE
  (5) JUNION SUITE UNTER DEN LINDEN
  (6) JUNION SUITE BRANDENBURG GATE
  (7) ADLON EXECUTIVE SUITE
  (8) BERLIN SUITE
  (9) LINDEN SUITE
  (10) PARISER PLATZ SUITE
  (11) ADLON DELUX SUITE
  (12) BRANDENBURG GATE DELUXE SUITE
  (13) IMPERIAL SUITE
  (14) PRESIDENTIAL SUITE BRANDENBURG GATE
  (15) ROYAL SUITE
  Which one would you like to book?`,
  promptConfirmation: `Perfect. I have all I need to complete the order. Would you like to proceed with the order?`,
  confirmOrder: 'Your order has been successful. In the next minutes you\'ll receive an E-mail with the details.',
  cancel: 'You have canceled the order.',
  journey: `That depends on how you prefer to travel. Going by taxi from Tegel
  (TXL) is ~25 € in approximately 20 minutes, from Schönefeld (SXF) its ~50 € in
  approximately 40 minutes.`,
  expenses: `We have a wide range of rooms available, if you want, I can guide you
through the booking process (at no cost to you). That way, you see the exact
price. Should we start the process?`,
  view: `Depending on where your room is, you can see both the Fernsehturm and the
Brandenburger Tor. Let's quickly go through finding the perfect room for you,
then we'll know for certain! Should we start the process?`,
};
