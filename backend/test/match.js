const assert = require('assert').strict;
const match = require('../src/match');
const responses = require('../src/responses');

describe('match', () => {
  describe('simple input without context', () => {
    it('defaults to sensible message without match', () => {
      const result = match({}, '');
      const expected = {
        context: {},
        response: responses.dontUnderstand,
      };
      assert.deepEqual(result, expected);
    });

    it('greets', () => {
      const inputs = ['hey', 'hello', 'how are you?', 'good day'];
      inputs.forEach(input => {
        const result = match({}, input);
        const expected = {
          context: {},
          response: responses.greeting,
        };
        assert.deepEqual(result, expected);
      });
    });

    it('describes how to get to the hotel', () => {
      const inputs = [
        'How do I get to the hotel?', 
        'how do i get there?', 
        'public transportation?'
      ];
      inputs.forEach(input => {
        const result = match({}, input);
        const expected = {
          context: {},
          response: responses.journey,
        };
        assert.deepEqual(result, expected);
      });
    });

    it('answers question about expense and offers initiating booking', () => {
      const inputs = [
        'how much for a room?', 
        'how expensive is a suite?', 
        'how much would i have to pay?',
        'whats the price for each room?',
      ];
      inputs.forEach(input => {
        const result = match({}, input);
        const expected = {
          context: {offeredBooking: true},
          response: responses.expenses,
        };
        assert.deepEqual(result, expected);
      });
    });

    it('answers question about view and offers initiating booking', () => {
      const inputs = [
        'can I see the brandenburger tor?', 
        'can I see the leipziger platz?', 
        'can I see unter den linden?', 
        'can I see the fernsehturm?', 
      ];
      inputs.forEach(input => {
        const result = match({}, input);
        const expected = {
          context: {offeredBooking: true},
          response: responses.view,
        };
        assert.deepEqual(result, expected);
      });
    });

    it('answers simple questions', () => {
      assert.deepEqual(
        match({}, 'can i smoke?'),
        {context: {}, response: responses.smokingPolicy},
      );

      assert.deepEqual(
        match({}, 'can i bring a pet?'),
        {context: {}, response: responses.petPolicy},
      );

      assert.deepEqual(
        match({}, 'is there free wifi?'),
        {context: {}, response: responses.wifi},
      );
    });

    it('offers to start the booking process from direct questions', () => {
      const inputs = [
        'Can I book a room?',
        'Could I rent a suite?',
        'I would like to get a room.',
        'I want to make a reservation.',
      ];

      inputs.forEach(input => {
        const result = match({}, input);
        const expected = {
          context: {offeredBooking: true},
          response: responses.offerBooking,
        };
        assert.deepEqual(result, expected);
      });
    });
  });

  describe('with context', () => {
    it('prompts for check-in when alice accepts the booking offer', () => {
      const context = {offeredBooking: true};
      const inputs = [ 'Sure!', 'Yes', 'Yup', 'Sounds good'];

      inputs.forEach(input => {
        const result = match(context, input);
        const expected = {
          context: {offeredBooking: false, isBooking: true},
          response: responses.promptCheckIn,
        };
        assert.deepEqual(result, expected);
      });
    });

    it('cancels the process at any time', () => {
      const tests = [
        {
          context: {isBooking: true, checkIn: '1'},
          input: 'i want to cancel'
        },
        {
          context: {
            isBooking: true,
            checkIn: '13th of nov',
            checkOut: 'the very next day',
            numberOfRooms: '1',
            adults: 'one',
            children: 'one',
            typeOfRoom: '1',
          },
          input: 'i want to cancel'
        },
      ];

      tests.forEach(({context, input}) => {
        const result = match(context, input);
        const expected = {
          context: {isBooking: false},
          response: responses.cancel,
        };

        assert.deepEqual(result, expected);
      });
    });

    it('prompts for check-out and updates check-in', () => {
      const context = {isBooking: true};
      const input = 'on the 13th of this month';

      const result = match(context, input);
      const expected = {
        context: {isBooking: true, checkIn: input},
        response: responses.promptCheckOut,
      };

      assert.deepEqual(result, expected);
    });

    it('prompts for room numbers and updates check-out', () => {
      const context = {isBooking: true, checkIn: '13th of nov'};
      const input = 'the day after that';

      const result = match(context, input);
      const expected = {
        context: Object.assign(context, {checkOut: input}),
        response: responses.promptRooms,
      };

      assert.deepEqual(result, expected);
    });

    it('prompts for number of adults and updates number of rooms', () => {
      const context = {
        isBooking: true,
        checkIn: '13th of nov',
        checkOut: 'the very next day'
      };
      const input = 'one room';

      const result = match(context, input);
      const expected = {
        context: Object.assign(context, {numberOfRooms: input}),
        response: responses.promptAdults,
      };

      assert.deepEqual(result, expected);
    });

    it('prompts for number of children and updates number of adults', () => {
      const context = {
        isBooking: true,
        checkIn: '13th of nov',
        checkOut: 'the very next day',
        numberOfRooms: '1',
      };
      const input = 'no adults';

      const result = match(context, input);
      const expected = {
        context: Object.assign(context, {adults: input}),
        response: responses.promptChildren,
      };

      assert.deepEqual(result, expected);
    });

    it('prompts for type of room and updates number of children', () => {
      const context = {
        isBooking: true,
        checkIn: '13th of nov',
        checkOut: 'the very next day',
        numberOfRooms: '1',
        adults: 'one',
      };
      const input = 'none';

      const result = match(context, input);
      const expected = {
        context: Object.assign(context, {children: input}),
        response: responses.promptTypeOfRoom,
      };

      assert.deepEqual(result, expected);
    });

    it('asks for confirmation and updates type of room', () => {
      const context = {
        isBooking: true,
        checkIn: '13th of nov',
        checkOut: 'the very next day',
        numberOfRooms: '1',
        adults: 'one',
        children: 'one',
      };
      const input = '1';

      const result = match(context, input);
      const expected = {
        context: Object.assign(context, {typeOfRoom: input}),
        response: responses.promptConfirmation,
      };

      assert.deepEqual(result, expected);
    });

    it('applies the order', () => {
      const context = {
        isBooking: true,
        checkIn: '13th of nov',
        checkOut: 'the very next day',
        numberOfRooms: '1',
        adults: 'one',
        children: 'one',
        typeOfRoom: '1',
      };
      const inputs = [ 'Sure!', 'Yes', 'Yup', 'Sounds good'];

      inputs.forEach(input => {
        const result = match(context, input);
        const expected = {
          context: {isBooking: false},
          response: responses.confirmOrder,
        };

        assert.deepEqual(result, expected);
      });
    });
  });
});

