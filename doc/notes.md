## Goals

Implementing a chat bot, hereafter called `Bob` (avoiding having to write "the
bot" all the time ...) that answers questions and guides a user, hereafter
called `Alice`, through her journey through the hotel booking process.

## Requirements

For the sake of simplicity of this exercise we have two categories of triggers,
each with a set of questions that map to responses or flows of conversation.
Every question should be interpreted with variations, the matching pattern
should be as general as possible. For example the question `How do I get to the
hotel?` might reasonably phrased `How do we get there?`.

On notation: plain text are straightforward questions, values in `[]` are types
or options, values in `()` are depending on context.

### Question

**Alice has not yet decided to book a room, Bob attempts to answer all her
questions and lead her to a call of action.**

Alice: How do I get to the hotel?
Bob: That depends on how you prefer to travel. Going by taxi from Tegel (TXL) is
~25 € in approximately 20 minutes, from Schönefeld (SXF) its ~50 € in
approximately 40 minutes.

Alice: How expensive is [a room/it]?
Bob: We have a wide range of rooms available, if you want, I can guide you
through the booking process (at no cost to you). That way, you see the exact
price. If you decided against booking, you can abort the process at any time.

Alice: Is there free [wi-fi/internet]?
Bob: Wi-Fi is available throughout the hotel.

Alice: Can I see the [Brandenburger Tor/Fernsehturm] from my room?
Bob: Depending on where your room is, you can see both the Fernsehturm and the
Brandenburger Tor. Let's quickly go through finding the perfect room for you,
then we'll know for certain!

Alice: Can I bring a pet?
Bob: Dogs are allowed in the hotel and can be brought along. We will charge
a 150 € cleaning fee in addition to the room rate, though dogs are not allowed
in the restaurants.

Alice: Can I smoke?
Bob: We offer a smokers lounge on the ground floor next to the reception, and
the Sra Bua Bar is open for smokers, but the rest of the hotel is no-smoking.

Alice: Would I have [...] in my room?
  *true* Bob: Yes, amongst [thing], you will have [... all the other things] in
  our rooms. Though, as that depends on the type of room though, let's find out
  which room will suite you best!
  *false* Bob: You have [... all the things] in your room, unfortunately we
  don't have [thing], however in booking you have the opportunity to request
  [thing] and the personal will do their best to accommodate you.

### Booking

**Alice decided to book a room, Bob will guide her through the booking
process.**

Alice: Can I book a room? (... or route from a previous question)
Bob: When do you want to check-in?
Alice: [~Date].

(In booking && has `check-in`)
Bob: When do you want to check-out?
Alice: [~Date].

(In booking && has `check-in`, `check-out`)
Bob: How many rooms do you need?
Alice: [~Number].

(In booking && has `check-in`, `check-out`, `numberOfRooms`)
Bob: How many adults will be staying in your room?
Alice: [~Number].

(In booking && has `check-in`, `check-out`, `numberOfRooms`, `numberOfAdults`)
Bob: And how many children will be staying in your room?
Alice: [~Number].

(In booking && has [... all the previous values])
Bob: Great! Now on to the room itself. We offer these: [... list all the rooms]. Which
one would you like to book?
Alice: [Name of room/Number/(first/second/nth)].

(In booking && has [... all the previous values])
Bob: Perfect. I have all I need to complete the order. Heres what you told me so
far: [list the booking information.] Would you like to proceed with the order or
cancel?

(whenever Alice responds with an unexpected format/value, i.e. a date)
Bob: I'm sorry, I didn't understand that, could you put the date in a format
like `year/day/month`, such as `2018/13/05`?

## Possible problems

- Alice might interupt some process (i.e. booking a room) by asking a
question. Bob should be able to pick up the process (e.g. by re-asking the
previous question) after answering the interupting question.
- Alice might make a mistake and require to change something stored in the
context.
- Alice might respond with an unparsable / incorrect value, Bob needs to prompt
her to retry and continue on success.
- Alice might want to (while in booking) ask about a thing Bob said, e.g. a
specific room type.
- Repetition of the same lines might be annoying, we could consider having
variants of the same line.

## Approach

### 1: Simple Stateless

- The responses are based entirely on user input, no context or state from
  conversation.
- The responses are selected from a fixed pool.
- The responses are selected on a first match basis, i.e. no weighing of
  responses.

Matching happens based on loose keyword-based patterns, e.g. if the words `book`
and `room` appear, we respond with a link to the booking page. Every input maps
to a corresponding response.

#### Benefits

- Straightforward and simple implementation.
- Easy to test.

#### Caveats

- Some conversations require follow-up questions/answers, i.e. the booking of a
  room, which is impossible with this simple, stateless model.
- No memory of previous answers means lots of accidental repetition in the
  conversation.
- Short-circuiting the matching means we are discarding possible matches further
  downstream.

### 2: Stateful

- The responses are selected both from mapping on questions, and/or on the
  context of previous answers/questions.
- The context might be a simple key/value store of properties such as
  `numberOfRooms`, or `pet`.

As we now have a context to base our responses off, we can have multiple
patterns of conversation:

1. alice question -> bob response

```
Alice: Could I bring a pet?
Bob: Dogs are not allowed in the hotel, but you may bring it to one of our
restaurants.
```

2. (alice question ->) (bob response,) bob question -> alice response (update context)

```
Alice: Could I bring a pet?
Bob: Dogs are not allowed in the hotel, but you may bring it to one of our
restaurants. Do you have a dog?
Alice: Yes, I have a dog.
```

3. (predicate context ->) bob prompt

```
Alice: Yes, I have a dog. (append 'dog' context.pets)
// predicate triggers question
Bob: If I may ask, what type of dog do you have?
// ...
```

We can now differenciate between units of dialogue:

**Alice question** triggers either a prompt or response from Bob.
**Bob response** does not trigger anything, leaves the user in control.
**Bob prompt** awaits an Alice response.
**Alice response** triggers prompt from Bob and/or a context update, it might
also fail to validate, in which case it might redo the previous prompt (maybe
with a slightly altered phrasing).

#### Benefits

- Relatively simple, as responses are a mapping of context + match to
  response.
- Easy to test.
- Unlike the stateless approach, allows for actual dialogue.

#### Caveats

- Short-circuiting of matches, in other words, we have to order the ruleset.

### 3: Further Ideas and pain points

1. Parsing dates and stuff is hard, probably better to trigger a UI element to
   off-load the responsibility of getting room number, check-in date, check-out
   date, etc. pp.

2. Some sort of weighing of matches might be useful (though probably harder to
   test).

3. `match` semantics are confusing, as the producer both returns the response
   and updates the context from the previous input. Maybe split up the
   responsibilities, and re-order execution: validate -> update -> respond.

4. Responses should probably be functions that close over the context, e.g. to
   show the current order progress.

5. After failing to parse questions N times, maybe offer a number of topics that
   succeed.

6. Should offer to explain, e.g. what the difference between every room is.
